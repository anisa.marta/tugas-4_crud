@extends('layout')

@section('judul')
	Registrasi
@endsection

@section('konten')

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>CRUD - Karyawan</title>
  </head>
  <body style="background-color: rgb(236, 228, 234)">
    @section('navbarregistrasi')
    active
    @endsection

            <br>
            
            <div class="col-10 offset-1" >
              <h3 align="center">Form Input Data Karyawan</h3>
              <div class="card">
                <div class="card-header text-white" style="background-color: rgb(0, 0, 0)">
                  Form Karyawan
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-12">
                        <form action="/simpan" method="POST">
                          {{ csrf_field() }}
                            <div class="mb-3">
                              <label for="nama-karyawan" class="form-label">Nama Karyawan</label>
                              <input type="text" class="form-control" name="nama_karyawan" placeholder="Input nama">
                            </div>
        
                            <div class="mb-3">
                                <label for="no-karyawan" class="form-label">No Karyawan</label>
                                <input type="number" class="form-control" name="no_karyawan" placeholder="Input nomor">
                            </div>
        
                            <div class="mb-3">
                                <label for="nohp-karyawan" class="form-label">No HP Karyawan</label>
                                <input type="text" class="form-control" name="no_telp_karyawan" placeholder="Input nomor telepon">
                            </div>
        
                            <div class="mb-3">
                                <label for="jabatan-karyawan" class="form-label">Jabatan Karyawan</label>
                                <select type="text" class="form-control" name="jabatan_karyawan" placeholder="Input jabatan">
                            </div>
        
                            <div class="mb-3">
                                <label for="divisi-karyawan" class="form-label">Divisi Karyawan</label>
                                <select type="text" class="form-control" name="divisi_karyawan" placeholder="Input divisi">
                                </select>
                                
                            </div><br>
                          
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </form>

                    </div>
        
                </div>
                </div>
              </div><br><br>
                  <br>
                  <h3 align="center">Hasil</h3>
               
                  
                    <div class="card-header text-white" style="background-color: rgb(0, 0, 0)">
                      Daftar Karyawan
                    </div>
                   
                      <table class="table table-bordered border-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nama Karyawan</th>
                            <th scope="col">No Karyawan</th>
                            <th scope="col">No Telp Karyawan</th>
                            <th scope="col">Jabatan Karyawan</th>
                            <th scope="col">Divisi Karyawan</th>
                            <th scope="col">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($kry as $item)
                          <tr>
                            <th scope="row">{{ $item->id }}</th>
                            <td>{{ $item->nama_karyawan }}</td>
                            <td>{{ $item->no_karyawan }}</td>
                            <td>{{ $item->no_telp_karyawan }}</td>
                            <td>{{ $item->jabatan_karyawan }} </td>
                            <td>{{ $item->divisi_karyawan }} </td>
                            <td>
                              <a href="/update/{{$item->id}}" class="btn btn-warning ">Edit</a>
                              <a onclick="return confirm('Yakin ingin menghapus data ?')" href="/delete/{{$item->id}}" class="btn btn-danger">Hapus</a>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                   
                  <br><br><br><br><br><br><br>

            </div>
            

            
        </div>
    <br><br><br><br><br><br><br><br><br><br><br><br></div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
@endsection