<!DOCTYPE html>
<html>
<head>
	<title>Anisa - @yield('judul')</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Jost:wght@400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/css/welcome.css">

	@yield('warna-navbar')

</head>

<body>
	<header>
		<nav>
            <div class="navbar navbar-expand-sm navbar-dark px-2" style="background-color: rgb(82, 0, 57)">
                <div class="container">
                  <div class="collapse navbar-collapse">
                    <ul class="navbar-nav">
                      <li class="nav-item">
                        <a class="nav-link @yield('navbarhome') " href="/">Home</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link @yield('navbarregistrasi')" href="/registrasi">Registrasi</a>
                      </li>
                     @yield('navbarupdate')
                    
                    
                  </div>
                </div>
              </div>
         
		</nav>
        @yield('konten')
	</header>
</body>
</html>