@extends('layout')

@section('judul')
	Update
@endsection

@section('konten')

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>CRUD - Edit</title>
  </head>
  <body style="background-color: rgb(236, 228, 234)">

    @section('navbarupdate')
    <li class="nav-item">
      <a class="nav-link active" href="/update">Update</a>
    </li>
    @endsection

            <br>
            
            <div class="col-11 mx-4" >
              <h5>Edit Data Karyawan</h5>
              <p>Silahkan edit form karyawan dibawah !</p>
              <div class="card">
                <div class="card-header bg-primary text-white">
                  Edit Data Karyawan
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-8">
                        
                        @foreach ($kry as $item)
                        <form action="/simpanUpdate" method="POST">
                          {{ csrf_field() }}
                            <div class="mb-3">
                                
                                <input type="hidden" class="form-control" name="id" value="{{$item->id}}">
                            </div>

                            <div class="mb-3">
                              <label for="nama-karyawan" class="form-label">Nama Karyawan</label>
                              <input type="text" class="form-control" name="nama_karyawan" value="{{$item->nama_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="no-karyawan" class="form-label">No Karyawan</label>
                                <input type="number" class="form-control" name="no_karyawan" value="{{$item->no_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="nohp-karyawan" class="form-label">No HP Karyawan</label>
                                <input type="text" class="form-control" name="no_telp_karyawan" value="{{$item->no_telp_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="jabatan-karyawan" class="form-label">Jabatan Karyawan</label>
                                <input type="text" class="form-control" name="jabatan_karyawan" value="{{$item->jabatan_karyawan}}">
                            </div>
        
                            <div class="mb-3">
                                <label for="divisi-karyawan" class="form-label">Divisi Karyawan</label>
                                <input type="text" class="form-control" name="divisi_karyawan" value="{{$item->divisi_karyawan}}">
                            </div><br>
                          
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <a href="/registrasi" class="btn btn-danger">Batal</a>
                          </form>
                          @endforeach
                    </div>
        
                </div>
                </div>
              </div><br><br><br><br><br>
            </div>
            

            
        </div>
    <br><br><br><br><br><br><br><br><br><br><br><br></div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
@endsection