<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class registrasiController extends Controller
{
    public function show(){

        $karyawan = DB::table('karyawan')->get();

        return view ('registrasi',['kry'=>$karyawan]);
    }

    public function prosesSimpan(Request $req){
        $nama_karyawan = $req->nama_karyawan;
        $no_karyawan = $req->no_karyawan;
        $no_telp_karyawan = $req->no_telp_karyawan;
        $jabatan_karyawan = $req->jabatan_karyawan;
        $divisi_karyawan = $req->divisi_karyawan;
        
        DB::table('karyawan')->insert(
            ['nama_karyawan'=>$nama_karyawan, 'no_karyawan'=>$no_karyawan, 'no_telp_karyawan'=>$no_telp_karyawan, 'jabatan_karyawan'=>$jabatan_karyawan, 'divisi_karyawan'=>$divisi_karyawan]
        );

        echo "<script>
        alert('Register Berhasil!');
        document.location.href='/registrasi';
        </script>";

    }
}


