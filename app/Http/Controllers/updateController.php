<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class updateController extends Controller
{

    public function edit($id){
        $karyawan = DB::table('karyawan')->where('id',$id)->get();

        return view('update',['kry' => $karyawan]);
    }

    public function prosesUpdate(Request $req){
        $nama_karyawan = $req->nama_karyawan;
        $no_karyawan = $req->no_karyawan;
        $no_telp_karyawan = $req->no_telp_karyawan;
        $jabatan_karyawan = $req->jabatan_karyawan;
        $divisi_karyawan = $req->divisi_karyawan;

        DB::table('karyawan')->where('id',$req->id)->update(
            ['nama_karyawan'=>$nama_karyawan, 'no_karyawan'=>$no_karyawan, 'no_telp_karyawan'=>$no_telp_karyawan, 'jabatan_karyawan'=>$jabatan_karyawan, 'divisi_karyawan'=>$divisi_karyawan]
        );

        echo "<script>
        alert('Update Data Berhasil!');
        document.location.href='/registrasi';
        </script>";
    }   

    public function prosesHapus($id){
        DB::table('karyawan')->where('id',$id)->delete();
		
		echo "<script>
        alert('Hapus Data Berhasil!');
        document.location.href='/registrasi';
        </script>";
    }

}
